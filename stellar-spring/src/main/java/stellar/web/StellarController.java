package stellar.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.logging.Logger;


@Controller
public class StellarController {

    private Logger logger = Logger.getLogger(StellarController.class.getName());

    @Autowired
    private StellarService service;

    @GetMapping("/systems")
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "Accept-Language", required = false) String acceptLanguage,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId){

        logger.info("Header Accept-Language:" + acceptLanguage);
        logger.info("Cookie JSESSIONID:" + sessionId);

        List<PlanetarySystem> systems = service.getSystems();

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId){
        logger.info("about to get planets of system " + systemId);
        model.addAttribute("planets", service.getPlanetsInSystem(systemId));
        model.addAttribute("system", service.getSystemById(systemId));
        return "planets";
    }


}
