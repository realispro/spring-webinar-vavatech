package stellar.dao;

import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface SystemDAO {

    List<PlanetarySystem> findAll();

    Optional<PlanetarySystem> findById(Integer id);

    PlanetarySystem addPlanetarySystem(PlanetarySystem system);

}
