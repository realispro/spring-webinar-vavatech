package stellar.dao.impl.inmemory;

import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class InMemoryPlanetDAO implements PlanetDAO {

    Logger logger = Logger.getLogger(InMemoryPlanetDAO.class.getName());

    @Override
    public List<Planet> findAll() {
        return InMemory.systems.stream().map(s->s.getPlanets()).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public List<Planet> findBySystem(PlanetarySystem system) {
        logger.info("fetching planets of " + system );

        return InMemory.systems.stream().filter(s->s.equals(system)).findFirst().get().getPlanets();
    }

    @Override
    public Optional<Planet> findById(Integer id) {
        return findAll().stream().filter(p->p.getId()==id).findFirst();
    }

    @Override
    public Planet addPlanet(Planet p) {
        int idMax = findAll().stream().sorted((p1, p2)->p2.getId()-p1.getId()).findFirst().get().getId();
        p.setId(++idMax);
        InMemory.systems.stream().filter(s->s.getId()==p.getSystem().getId()).findFirst().get().getPlanets().add(p);
        return p;
    }
}
