package stellar.dao.impl.inmemory;

import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;


public class InMemorySystemDAO implements SystemDAO {

    @Override
    public List<PlanetarySystem> findAll() {
        return InMemory.systems;
    }


    @Override
    public Optional<PlanetarySystem> findById(Integer id) {
        return InMemory.systems.stream().filter(s->s.getId()==id).findFirst();
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        int idMax = InMemory.systems.stream().sorted((s1,s2)->s2.getId()-s1.getId()).findFirst().get().getId();

        system.setId(++idMax);
        InMemory.systems.add(system);
        return system;
    }
}