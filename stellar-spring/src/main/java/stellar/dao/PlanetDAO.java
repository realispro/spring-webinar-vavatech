package stellar.dao;

import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface PlanetDAO  {

    List<Planet> findAll();

    List<Planet> findBySystem(PlanetarySystem system);

    Optional<Planet> findById(Integer id);

    Planet addPlanet(Planet p);

}
