package stellar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import stellar.service.StellarService;

@Configuration
@ComponentScan("stellar")
public class StellarApplication {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarApplication.class);

        StellarService service = context.getBean(StellarService.class);
        service.getSystems().forEach(System.out::println);

    }
}
