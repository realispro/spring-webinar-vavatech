package stellar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@Component
public class SimpleStellarService implements StellarService{

    @Autowired
    private SystemDAO systemDAO;

    @Autowired
    private PlanetDAO planetDAO;

    @Override
    public List<PlanetarySystem> getSystems() {
        return systemDAO.findAll();
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        return systemDAO.findById(id).orElse(null);
    }

    @Override
    public List<Planet> getPlanets() {
        return planetDAO.findAll();
    }

    @Override
    public List<Planet> getPlanetsInSystem(int systemId) {
        return planetDAO.findBySystem(systemDAO.findById(systemId).get());
    }

    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.findById(id).orElse(null);
    }
}
