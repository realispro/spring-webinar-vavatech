package stellar.service;

import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface StellarService {

    List<PlanetarySystem> getSystems();

    PlanetarySystem getSystemById(int id);

    List<Planet> getPlanets();

    List<Planet> getPlanetsInSystem(int systemId);

    Planet getPlanetById(int id);


}
