package stellar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;
import java.sql.Driver;

@Configuration
public class StellarConfig {

    @Bean
    DataSource dataSource() throws ClassNotFoundException {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass((Class<? extends Driver>) Class.forName("org.h2.Driver"));
        dataSource.setUrl("jdbc:h2:tcp://localhost/~/stellar-webinar");
        dataSource.setUsername("sa");
        dataSource.setPassword("");
        return dataSource;
    }

}
