create table planetarysystem (
   id int primary key auto_increment,
   name varchar,
   star varchar,
   distance float,
   discovery timestamp
);

create table planet (
  id int primary key auto_increment,
  name varchar,
  system_id int
);

insert into planetarysystem (id,name,star,distance,discovery) values (1,'Solar System','Sun',1.11,'1212-02-13');
insert into planetarysystem (id,name,star,distance,discovery) values (2,'Kepler-90 System','Kepler-90',2.567,'2012-06-23');
insert into planetarysystem (id,name,star,distance,discovery) values (3,'Trappist-1 System','Trappist-1',13.121,'2016-01-12');
insert into planetarysystem (id,name,star,distance,discovery) values (4,'Kepler-11 System','Kepler-11',7.61,'2019-02-13');

insert into planet (id,name,system_id) values (1, 'Mercury', 1);
insert into planet (id,name,system_id) values (12, 'Venus', 1);
insert into planet (id,name,system_id) values (13, 'Earth', 1);
insert into planet (id,name,system_id) values (14, 'Mars', 1);
insert into planet (id,name,system_id) values (15, 'Jupiter', 1);
insert into planet (id,name,system_id) values (16, 'Neptun', 1);
insert into planet (id,name,system_id) values (17, 'Saturn', 1);
insert into planet (id,name,system_id) values (18, 'Uranus', 1);
insert into planet (id,name,system_id) values (19, 'Pluto', 1);

