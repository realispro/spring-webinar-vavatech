package stellar.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface SystemDAO extends JpaRepository<PlanetarySystem, Integer> {

    List<PlanetarySystem> findAll();

    Optional<PlanetarySystem> findById(Integer id);

}
