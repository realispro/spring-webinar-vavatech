package stellar.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.Optional;

public interface PlanetDAO extends JpaRepository<Planet, Integer> {

    List<Planet> findAll();

    List<Planet> findBySystem(PlanetarySystem system);

    Optional<Planet> findById(Integer id);

}
